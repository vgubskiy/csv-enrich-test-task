package ru.rgs.csvparser.service.impl;

import feign.FeignException;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rgs.csvparser.model.Client;
import ru.rgs.csvparser.model.Score;
import ru.rgs.csvparser.feign.ClientService;
import ru.rgs.csvparser.service.CsvParserService;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CsvParserServiceImpl implements CsvParserService {

    private ClientService clientService;

    public CsvParserServiceImpl() {
    }

    @Autowired
    public CsvParserServiceImpl(ClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    @SneakyThrows
    public Path processCsv(Path source) {
        String outputPath = source.toString().replace("input_", "output_");
        List<String> outputData = Files.readAllLines(source)
                                    .parallelStream().skip(1)
                                    .map(s -> s.split(","))
                                    .map(s -> new Client(parseName(s), LocalDate.parse(s[3])))
                                    .map(client -> {
                                        StringBuilder outStrBuilder = new StringBuilder();
                                        outStrBuilder.append(client.getClientName())
                                                .append(",")
                                                .append(client.getContractDate());
                                        try {
                                            Score score = clientService.getClientScore(client);
                                            if (score.getScoringValue() == null) {
                                                outStrBuilder.append(",не найден");
                                            } else {
                                                outStrBuilder.append(",").append(score.getScoringValue());
                                            }
                                        } catch (FeignException f) {
                                            outStrBuilder.append(",ошибка обработки");
                                        }
                                        return outStrBuilder.toString();
                                    })
                                    .collect(Collectors.toList());

        outputData.add(0, "CLIENT_NAME,CONTRACT_DATE,SCORING");
        Files.write(Paths.get(outputPath), outputData);
        return Paths.get(outputPath);
    }

    private String parseName(String[] line) {
        return line[0].toUpperCase() + " " + line[2].toUpperCase() + " " + line[1].toUpperCase();
    }
}
