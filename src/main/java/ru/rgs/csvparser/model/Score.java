package ru.rgs.csvparser.model;

import lombok.Data;

@Data
public class Score {
    private String status;
    private Double scoringValue;
}
