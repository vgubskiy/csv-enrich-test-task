package ru.rgs.csvparser.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import ru.rgs.csvparser.model.Client;
import ru.rgs.csvparser.model.Score;

@FeignClient(url = "http://localhost:8081", name = "client-service")
public interface ClientService {

    /**
     * Get the scoring information for the client from some microservice.
     * @param client - the client object
     * @return th Score object
     */
    @PostMapping("/score")
    Score getClientScore(Client client);
}
